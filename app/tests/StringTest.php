<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 15:19
 */

class StringTest extends PHPUnit_Framework_TestCase
{
    public function setUp() {
        $this->abc = new String("abc");
    }

    public function tearDown() {
        unset($this->abc);
    }


    public function testToString()
    {
        $result = $this->abc->toString('contains %s');
        $expected = 'contains abc';

        $this->assertTrue($result == $expected);
    }


    function testCopy() {
        $abc2 = $this->abc->copy();

        $this->assertEquals($abc2, $this->abc);
    }


    function testConcat() {
        $string = new String('123');
        $this->abc->concat($string);

        $result = $this->abc->toString();
        $expected = "abc123";

        $this->assertTrue($result == $expected);
    }


    public function testEmail()
    {
        $email = 'me@me.com';
        $subject = 'My subject';

        $stub = $this->getMock('Emailer');
        $stub->expects($this->once())
            ->method('send')
            ->with( $this->equalTo($email), $this->equalTo($subject) )
            ->will($this->returnValue(true));

        $string = new String('my string', $stub);

        $this->assertTrue($string->email($email,$subject));
    }
}
