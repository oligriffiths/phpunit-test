<?php
/**
 * User: Oli Griffiths <oli@expandtheroom.com>
 * Date: 25/02/2013
 * Time: 11:46
 */

class String
{
    //contains the internal data
    protected $data;

    protected $emailer;

    // constructor
    public function __construct($data, Emailer $emailer = null)
    {
        $this->data = $data;
        $this->emailer = $emailer;
    }

    public function toString($format = '%s')
    {
        return sprintf($format, $this->data);
    }


    public function __toString()
    {
        return $this->toString();
    }


    public function copy()
    {
        $class = get_class($this);
        $new = new $class($this->data);
        return $new;
    }


    public function concat($string)
    {
        $this->data .= $string;
    }

    public function email($email, $subject)
    {
        return $this->emailer->send($email, $subject, $this->data);
    }
}